At Proper, we understand that a quality dish starts with fresh, high-quality, locally-sourced ingredients. We offer a number of traditional, vegan, and gluten-free options. Available for take-away or delivery.

Address: 1011 S Alvarado Street, Los Angeles, CA 90006, USA

Phone: 213-568-3395

Website: http://eatproperpizza.com
